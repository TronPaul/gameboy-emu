Little Endian
(hl) -> value in memory at hl

## Registers

| 15..8 |      | 7..0 |
|:-----:|------|:----:|
|   A   |      |  F   |
|   B   |      |  C   |
|   D   |      |  E   |
|   H   |      |  L   |
|       | SP   |      |
|       | PC   |      |

### Flag Register

| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|---|---|---|---|---|---|---|---|
| Z | N | H | C | 0 | 0 | 0 | 0 |

* Z - Zero Flag
* N - Subtract Flag
* H - Half Carry Flag
* C - Carry Flag

## Memory

|      Desc                 | Location |
|:--------------------------|:--------:|
| Interrupt Enable Register | FFFF |
| Internal RAM | FFFE-FF80|
| Empty, but unusable | FF79-FF4C|
| I/O Ports | FF4B-FF00 |
| Empty, but unusable | FEFF-FEA0 |
| Sprite Attrib Memory (OAM) | FE9F-FE00 |
| Echo of 8kb Internal RAM | FDFF-E000 |
| 8kb Internal RAM | DFFF-C000 |
| 8kb switchable RAM bank | BFFF-A000 |
| 8kb Video RAM | 9FFF-8000 |
| 32k Cartridge | 7FFF-0000 |