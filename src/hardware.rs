use std::ops::{Index, IndexMut};

pub struct Registers {
    pub a: u8, pub f: u8,
    pub b: u8, pub c: u8,
    pub d: u8, pub e: u8,
    pub h: u8, pub l: u8,
    pub sp: u16, pub pc: u16
}

pub fn default_registers() -> Registers {
    Registers {
        a: 0, f: 0,
        b: 0, c: 0,
        d: 0, e: 0,
        h: 0, l: 0,
        sp: 0, pc: 0 // PC should be 0x100
    }
}

pub enum Register {
    A, B, C, D, E, H, L
}

pub struct MMU {
    pub ram: [u8; 0x4000], pub rom: [u8; 0x8000]
}

impl Index<u16> for MMU {
    type Output = u8;

    fn index(&self, index: u16) -> &Self::Output {
        if index < 0x8000 {
            return &self.rom[index as usize];
        } else {
            panic!()
        }
    }
}

impl IndexMut<u16> for MMU {
    fn index_mut(&mut self, index: u16) -> &mut Self::Output {
        if index < 0x8000 {
            return &mut self.rom[index as usize];
        } else {
            panic!()
        }
    }
}