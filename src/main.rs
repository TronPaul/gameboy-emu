mod hardware;
use crate::hardware::*;
use crate::hardware::Register::*;
use crate::Instruction::*;

enum Instruction {
    ADD(Register),
    SUB(Register),
    LD(Register, Register)
}

struct GameBoy {
    registers: Registers,
    mmu: MMU
}

impl GameBoy {
    fn instruction_from_byte(&mut self, b: u8) -> Option<Instruction> {
        match b {
            0x80 => Some(ADD(B)),
            0x81 => Some(ADD(C)),
            0x82 => Some(ADD(D)),
            0x83 => Some(ADD(E)),
            0x84 => Some(ADD(H)),
            0x85 => Some(ADD(L)),
            0x87 => Some(ADD(A)),
            0x90 => Some(SUB(B)),
            0x91 => Some(SUB(C)),
            0x92 => Some(SUB(D)),
            0x93 => Some(SUB(E)),
            0x94 => Some(SUB(H)),
            0x95 => Some(SUB(L)),
            0x97 => Some(SUB(A)),
            0x40 => Some(LD(B, B)),
            0x41 => Some(LD(B, C)),
            0x42 => Some(LD(B, D)),
            0x43 => Some(LD(B, E)),
            0x44 => Some(LD(B, H)),
            0x45 => Some(LD(B, L)),
            0x47 => Some(LD(B, A)),
            0x48 => Some(LD(C, B)),
            0x49 => Some(LD(C, C)),
            0x4A => Some(LD(C, D)),
            0x4B => Some(LD(C, E)),
            0x4C => Some(LD(C, H)),
            0x4D => Some(LD(C, L)),
            0x4F => Some(LD(C, A)),
            0x50 => Some(LD(D, B)),
            0x51 => Some(LD(D, C)),
            0x52 => Some(LD(D, D)),
            0x53 => Some(LD(D, E)),
            0x54 => Some(LD(D, H)),
            0x55 => Some(LD(D, L)),
            0x57 => Some(LD(D, A)),
            0x58 => Some(LD(E, B)),
            0x59 => Some(LD(E, C)),
            0x5A => Some(LD(E, D)),
            0x5B => Some(LD(E, E)),
            0x5C => Some(LD(E, H)),
            0x5D => Some(LD(E, L)),
            0x5F => Some(LD(E, A)),
            0x60 => Some(LD(H, B)),
            0x61 => Some(LD(H, C)),
            0x62 => Some(LD(H, D)),
            0x63 => Some(LD(H, E)),
            0x64 => Some(LD(H, H)),
            0x65 => Some(LD(H, L)),
            0x67 => Some(LD(H, A)),
            0x68 => Some(LD(L, B)),
            0x69 => Some(LD(L, C)),
            0x6A => Some(LD(L, D)),
            0x6B => Some(LD(L, E)),
            0x6C => Some(LD(L, H)),
            0x6D => Some(LD(L, L)),
            0x6F => Some(LD(L, A)),
            0x78 => Some(LD(A, B)),
            0x79 => Some(LD(A, C)),
            0x7A => Some(LD(A, D)),
            0x7B => Some(LD(A, E)),
            0x7C => Some(LD(A, H)),
            0x7D => Some(LD(A, L)),
            0x7F => Some(LD(A, A)),
            _ => None
        }
    }

    fn step(&mut self) {
        match self.instruction_from_byte(self.mmu[self.registers.pc]) {
            Some(ins) => self.execute(ins),
            None => panic!()
        }
    }

    fn execute(&mut self, instruction: Instruction) {
        match instruction {
            ADD(A) => self.add(self.registers.a),
            ADD(B) => self.add(self.registers.b),
            ADD(C) => self.add(self.registers.c),
            ADD(D) => self.add(self.registers.d),
            ADD(E) => self.add(self.registers.e),
            ADD(H) => self.add(self.registers.h),
            ADD(L) => self.add(self.registers.l),
            SUB(A) => self.sub(self.registers.a),
            SUB(B) => self.sub(self.registers.b),
            SUB(C) => self.sub(self.registers.c),
            SUB(D) => self.sub(self.registers.d),
            SUB(E) => self.sub(self.registers.e),
            SUB(H) => self.sub(self.registers.h),
            SUB(L) => self.sub(self.registers.l),
            LD(A, A) => self.ld(A, self.registers.a),
            LD(A, B) => self.ld(A, self.registers.b),
            LD(A, C) => self.ld(A, self.registers.c),
            LD(A, D) => self.ld(A, self.registers.d),
            LD(A, E) => self.ld(A, self.registers.e),
            LD(A, H) => self.ld(A, self.registers.h),
            LD(A, L) => self.ld(A, self.registers.l),
            LD(B, A) => self.ld(B, self.registers.a),
            LD(B, B) => self.ld(B, self.registers.b),
            LD(B, C) => self.ld(B, self.registers.c),
            LD(B, D) => self.ld(B, self.registers.d),
            LD(B, E) => self.ld(B, self.registers.e),
            LD(B, H) => self.ld(B, self.registers.h),
            LD(B, L) => self.ld(B, self.registers.l),
            LD(C, A) => self.ld(C, self.registers.a),
            LD(C, B) => self.ld(C, self.registers.b),
            LD(C, C) => self.ld(C, self.registers.c),
            LD(C, D) => self.ld(C, self.registers.d),
            LD(C, E) => self.ld(C, self.registers.e),
            LD(C, H) => self.ld(C, self.registers.h),
            LD(C, L) => self.ld(C, self.registers.l),
            LD(D, A) => self.ld(D, self.registers.a),
            LD(D, B) => self.ld(D, self.registers.b),
            LD(D, C) => self.ld(D, self.registers.c),
            LD(D, D) => self.ld(D, self.registers.d),
            LD(D, E) => self.ld(D, self.registers.e),
            LD(D, H) => self.ld(D, self.registers.h),
            LD(D, L) => self.ld(D, self.registers.l),
            LD(E, A) => self.ld(E, self.registers.a),
            LD(E, B) => self.ld(E, self.registers.b),
            LD(E, C) => self.ld(E, self.registers.c),
            LD(E, D) => self.ld(E, self.registers.d),
            LD(E, E) => self.ld(E, self.registers.e),
            LD(E, H) => self.ld(E, self.registers.h),
            LD(E, L) => self.ld(E, self.registers.l),
            LD(H, A) => self.ld(H, self.registers.a),
            LD(H, B) => self.ld(H, self.registers.b),
            LD(H, C) => self.ld(H, self.registers.c),
            LD(H, D) => self.ld(H, self.registers.d),
            LD(H, E) => self.ld(H, self.registers.e),
            LD(H, H) => self.ld(H, self.registers.h),
            LD(H, L) => self.ld(H, self.registers.l),
            LD(L, A) => self.ld(L, self.registers.a),
            LD(L, B) => self.ld(L, self.registers.b),
            LD(L, C) => self.ld(L, self.registers.c),
            LD(L, D) => self.ld(L, self.registers.d),
            LD(L, E) => self.ld(L, self.registers.e),
            LD(L, H) => self.ld(L, self.registers.h),
            LD(L, L) => self.ld(L, self.registers.l)
        }
    }

    fn add(&mut self, source: u8) {
        let (result, overflow) = self.registers.a.overflowing_add(source);
        let half_overflow = (self.registers.a & 0xF) + (source & 0xF) > 0xF;
        self.set_flags(result == 0, false, half_overflow, overflow);
        self.registers.a = result;
        self.registers.pc += 1;
    }

    fn sub(&mut self, source: u8) {
        // TODO find out if overflow on sub == borrow
        let (result, full_overflow) = self.registers.a.overflowing_sub(source);
        let mut mask: u8 = 0x80;
        let mut half_borrow = false;
        let mut borrow = false;
        loop {
            if (self.registers.a & mask) < (source & mask) {
                if mask <= 0x08 {
                    half_borrow = true;
                }
                borrow = true;
            }
            if mask == 1 || (half_borrow && borrow) {
                break
            }
            mask >>= 1;
        }
        self.set_flags(result == 0, true, half_borrow, borrow);
        self.registers.a = result;
        self.registers.pc += 1;
    }

    fn ld(&mut self, target: Register, source: u8) {
        match target {
            A => self.registers.a = source,
            B => self.registers.b = source,
            C => self.registers.c = source,
            D => self.registers.d = source,
            E => self.registers.e = source,
            H => self.registers.h = source,
            L => self.registers.l = source,
        }
        self.registers.pc += 1;
    }

    fn set_flags(&mut self, zero: bool, subtract: bool, half_overflow: bool, overflow: bool) {
        if zero {
            self.registers.f |= 0x80
        } else {
            self.registers.f &= 0x7F
        }
        if subtract {
            self.registers.f |= 0x40;
        } else {
            self.registers.f &= 0xBF;
        }
        if half_overflow {
            self.registers.f |= 0x20
        } else {
            self.registers.f &= 0xDF
        }
        if overflow {
            self.registers.f |= 0x10
        } else {
            self.registers.f &= 0xEF
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::GameBoy;
    use crate::hardware::{MMU, default_registers, Register};

    #[test]
    fn add_both_overflow() {
        let mut gameboy = GameBoy {
            registers: default_registers(),
            mmu: MMU {
                ram: [0; 0x4000],
                rom: [0; 0x8000]
            }
        };
        gameboy.registers.a = 0xFF;
        gameboy.add(0xFF);
        assert_eq!(0xFE, gameboy.registers.a);
        assert_eq!(0x30, gameboy.registers.f);
        assert_eq!(1, gameboy.registers.pc);
    }

    #[test]
    fn add_zero() {
        let mut gameboy = GameBoy {
            registers: default_registers(),
            mmu: MMU {
                ram: [0; 0x4000],
                rom: [0; 0x8000]
            }
        };
        gameboy.add(0);
        assert_eq!(0, gameboy.registers.a);
        assert_eq!(0x80, gameboy.registers.f);
        assert_eq!(1, gameboy.registers.pc);
    }

    #[test]
    fn load() {
        let mut gameboy = GameBoy {
            registers: default_registers(),
            mmu: MMU {
                ram: [0; 0x4000],
                rom: [0; 0x8000]
            }
        };
        gameboy.registers.a = 4;
        gameboy.ld(Register::A, 8);
        assert_eq!(8, gameboy.registers.a);
        assert_eq!(1, gameboy.registers.pc);
    }

    #[test]
    fn subtract() {
        let mut gameboy = GameBoy {
            registers: default_registers(),
            mmu: MMU {
                ram: [0; 0x4000],
                rom: [0; 0x8000]
            }
        };
        gameboy.registers.a = 0;
        gameboy.sub(0xFF);
        assert_eq!(1, gameboy.registers.a);
        assert_eq!(0x70, gameboy.registers.f);
        assert_eq!(1, gameboy.registers.pc);
    }

    #[test]
    fn subtract_zero() {
        let mut gameboy = GameBoy {
            registers: default_registers(),
            mmu: MMU {
                ram: [0; 0x4000],
                rom: [0; 0x8000]
            }
        };
        gameboy.registers.a = 0;
        gameboy.sub(0);
        assert_eq!(0, gameboy.registers.a);
        assert_eq!(0xC0, gameboy.registers.f);
        assert_eq!(1, gameboy.registers.pc);
    }
}

fn main() {
    println!("Hello, world!");
}
